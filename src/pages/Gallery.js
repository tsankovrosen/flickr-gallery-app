import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withLayout} from '../components/hoc';
import {TagInputContainer, OrderByContainer} from '../containers';
import GalleryList from '../components/gallery/GalleryList';
import {Loading} from '../components/common';
import {Row} from 'react-bootstrap';

class Gallery extends Component {
  componentDidMount() {
    this.props.fetchAll();
  }

  render() {
    const {
      items,
      loading
    } = this.props;

    return (
      <>
        <div className="filter-area-container">
          <Row>
            <div className="tag-input-additional-description">
              You can search with multiple tags
            </div>
          </Row>
          <Row>
            <TagInputContainer/>
            <OrderByContainer/>
          </Row>
        </div>
        {loading ? <Loading/> : <GalleryList items={items}/>}
      </>
    );
  }
}

Gallery.propTypes = {
  items: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired
};


export default withLayout()(Gallery);


