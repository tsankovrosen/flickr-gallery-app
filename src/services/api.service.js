import axios from 'axios';

const API_KEY = 'bac3c7917ad594af106678377a9c50e1';
const SECRET = 'ad60f260105a056a';
const API_URL = 'https://api.flickr.com/services/feeds/photos_public.gne';
const settings = {
  api_key: API_KEY,
  secret: SECRET,
  format: 'json',
  nojsoncallback: 1
};

export const Api = {
  getAll
};

async function getAll(criteria) {
  return await axios.get(API_URL, {
    params: {
      ...settings,
      ...criteria
    }
  });
}
