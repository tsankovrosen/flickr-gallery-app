import React from 'react';
import {LayoutContainer} from "containers";

export default withLayout;

function withLayout(layoutSettings = {}, Layout = LayoutContainer) {
  return RenderedComponent => {
    const WithLayoutHOC = props => {
      return (
        <Layout {...layoutSettings}>
          <RenderedComponent {...props}/>
        </Layout>
      );
    };
    
    const wrappedComponentName = Layout.displayName
      || Layout.name
      || 'LayoutComponent';

    WithLayoutHOC.displayName = `WithLayoutHOC(${wrappedComponentName})`;

    return WithLayoutHOC;
  }
}