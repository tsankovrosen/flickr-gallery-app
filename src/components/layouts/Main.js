import React from 'react';

function Main({children}) {
  return (
    <section className='container main-container'>
      {children}
    </section>
  );
}

export default Main;