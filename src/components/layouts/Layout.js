import React from 'react';
import PropTypes from 'prop-types';
import {Main, Header} from './';
import {Alert} from "../common";

function Layout({children, alert}) {
  return (
    <>
      <Header/>

      {alert.message
        && <Alert type={alert.type}>{alert.message}</Alert>}

      <Main>
        {children}
      </Main>
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.element.isRequired
};

export default Layout;