import React from 'react';
import {Navbar} from 'react-bootstrap';

function Header() {
  return (
    <header className="header-container">
      <Navbar
        expand="lg"
        variant="light"
        bg="light">
        <Navbar.Brand href="#">RTS App - Flickr Gallery</Navbar.Brand>
      </Navbar>
    </header>
  );
}

export default Header;