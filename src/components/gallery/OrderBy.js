import React from 'react';
import PropTypes from 'prop-types';

const orderByValues = {
  published: 'Published',
  date_taken: 'Date Taken'
};

const OrderBy = ({orderBy, orderByValue}) => {
  const handleChange = (event) => {
    orderBy(event.target.value);
  };

  return (
    <div className="order-by-dropdown-container">
      <select
        className="form-input"
        value={orderByValue}
        onChange={handleChange}>
        <option value="">Order By ...</option>
        {Object
          .keys(orderByValues)
          .map((name) => {
            const value = orderByValues[name];

            return <option
              name={name}
              key={name}
              value={name}>{value}</option>
          })}
      </select>
    </div>
  );
};

OrderBy.propTypes = {
  orderBy: PropTypes.func.isRequired,
  orderByValues: PropTypes.string
};

export default OrderBy;
