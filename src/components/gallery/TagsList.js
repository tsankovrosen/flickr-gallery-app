import React from 'react';
import Tag from './Tag';
import PropTypes from 'prop-types';

function TagsList({tags}) {
  const renderTags = (tags) => {
    tags = tags.split(" ").filter(tag => tag);

    return tags.length === 0
      ? 'No tags'
      : tags.map(tagName => (
        <Tag
          key={tagName}
          tagName={tagName}
        />
      ));
  };

  return renderTags(tags);
}

TagsList.propTypes = {
  tags: PropTypes.string.isRequired
};

export default TagsList;