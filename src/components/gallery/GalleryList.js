import React from 'react';
import PropTypes from 'prop-types';
import GalleryListItem from './GalleryListItem';
import {Grid, Row} from 'react-bootstrap';

function GalleryList({items}) {
  return (
    <div className="gallery-list">
      <Grid>
        <Row className="show-grid">
          {items.map((item, index) => {
            return (<GalleryListItem
              key={index}
              {...item}
            />)
          })}
        </Row>
      </Grid>
    </div>
  );
}

GalleryList.propTypes = {
  items: PropTypes.array.isRequired
};

export default GalleryList;
