import React from 'react';
import PropTypes from 'prop-types';

function TagInput({
  tagName,
  setValue,
  findByTagName
}) {
  const handleSubmit = (event) => {
    event.preventDefault();

    findByTagName(tagName.replace(/\s+/g, ','));
  };

  return (
    <form
      noValidate
      onSubmit={e => handleSubmit(e)}>
      <input
        placeholder="example: Instagram ios photos"
        className="from-input"
        type='text'
        value={tagName}
        onChange={(e) => setValue(e.target.value)}
      />
    </form>
  );
}

TagInput.propTypes = {
  tagName: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  findByTagName: PropTypes.func.isRequired
};

export default TagInput;
