import React from 'react';
import PropTypes from 'prop-types';

const Tag = ({tagName}) => {
  return (
    <span className="tag-name">{tagName}</span>
  );
};

Tag.propTypes = {
    tagName: PropTypes.string.isRequired
};

export default Tag;
