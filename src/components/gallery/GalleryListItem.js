import React from 'react';
import PropTypes from 'prop-types';
import {Col} from 'react-bootstrap';
import TagsList from './TagsList'

function GalleryListItem({
  media,
  title,
  author,
  published,
  tags,
}) {
  const src = media.m;

  const parseDate = (published) => {
    return new Date(published).toString();
  };

  return (
    <Col xs={12} md={6}>
      <div className="gallery-list-item">
        <div className="image-container">
          <img
            src={src}
            alt={title}
          />
        </div>
        <div className="metadata">
          <ul className="metadata-details">
            <li>Author: <strong>{author}</strong></li>
            <li>Title: <strong>{title}</strong></li>
            <li>Published: {parseDate(published)}</li>
            <li>Tags: {<TagsList tags={tags}/>}</li>
          </ul>
        </div>
        <div className="image-save">
          <a
            href={src}
            onClick={() => window.open(src)}
            download>Download</a>
        </div>
      </div>
    </Col>
  );
}

GalleryListItem.propTypes = {
  media: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  author:  PropTypes.string.isRequired,
  published:  PropTypes.string.isRequired,
  tags: PropTypes.string,
};

export default GalleryListItem;
