import React from 'react';
import {GalleryContainer} from "../containers";
import {Provider} from "react-redux";
import configureStore from "../redux/store";

import 'bootstrap/dist/css/bootstrap.css';
import '../static/index.scss';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <div className="app-container">
        <GalleryContainer />
      </div>
    </Provider>
  );
}

export default App;