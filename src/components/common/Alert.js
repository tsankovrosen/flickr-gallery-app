import React from "react";
import PropTypes from 'prop-types';

function Alert({children, type}) {
  return (
    <div className="container alert-container">
      <div className={`alert alert-${type}`}>
        {children}
      </div>
    </div>
  )
}

Alert.propTypes = {
  type: PropTypes.string.isRequired
};

export default Alert;