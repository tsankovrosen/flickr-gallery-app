import {connect} from 'react-redux';
import {fetchAll} from '../redux/actions';
import {Gallery} from '../pages';

function mapStateToProps({api, alert}) {
  return {
    items: api.items,
    loading: api.loading,
    alert,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAll: () => dispatch(fetchAll())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Gallery);

