import {Layout} from '../components/layouts';
import {connect} from 'react-redux';

function mapStateToProps({alert}) {
  return {
    alert
  }
}

export default connect(mapStateToProps)(Layout);