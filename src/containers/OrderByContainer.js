import {connect} from 'react-redux';
import {orderBy} from '../redux/actions';
import OrderBy from '../components/gallery/OrderBy';

function mapStateToProps(state) {
  return {
    orderByValue: state.api.orderBy
  }
}

function mapDispatchToProps(dispatch) {
  return {
    orderBy: orderByInput => dispatch(orderBy(orderByInput)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderBy);

