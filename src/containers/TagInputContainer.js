import {connect} from 'react-redux';
import {
  fetchAll,
  setValue,
} from '../redux/actions';
import TagInput from '../components/gallery/TagInput';

function mapStateToProps(state) {
  return {
    tagName: state.api.tagName
  };
}

function mapDispatchToProps(dispatch) {
  return {
    findByTagName: tagName => dispatch(fetchAll({
      tags: tagName
    })),
    setValue: tagName => dispatch(setValue(tagName))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagInput);

