import alertReducer from '../redux/reducers/alert.reducer';
import {successAlert, errorAlert, clearAlert} from '../redux/actions';

const alertTypes = {
  success: 'success',
  error: 'danger',
};

it('should test success alert state', () => {
  const message = 'success test';
  const state = alertReducer({}, successAlert(message));

  expect(state.message).toEqual(message);
  expect(state.type).toEqual(alertTypes.success);
});

it('should test error/danger alert state', () => {
  const message = 'danger test';
  const state = alertReducer({}, errorAlert(message));

  expect(state.message).toEqual(message);
  expect(state.type).toEqual(alertTypes.error);
});

it('should test clear alert state', () => {
  const state = alertReducer({}, clearAlert());

  expect(state).toEqual({});
});