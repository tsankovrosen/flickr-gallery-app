import apiResponse from './apiResponse';
import apiReducer from '../redux/reducers/api.reducer';
import * as actions from '../redux/actions';
import {API_CONSTANTS} from "../redux/constants";


it('should set tag name', () => {
  const tagName = 'test';
  const state = apiReducer({}, actions.setValue(tagName));

  expect(state.tagName).toEqual(tagName);
});

it('should change state of the loading to true', () => {
  const state = apiReducer({}, actions.startLoading());

  expect(state.loading).toEqual(true);
});

it('should fetch the data from api', () => {
  const action = {
    type: API_CONSTANTS.FETCH_SUCCESS_ALL_DATA,
    payload: apiResponse
  };

  const state = apiReducer({}, action);

  expect(state.items.length).toEqual(20);
  expect(state.error).toEqual(false);
  expect(state.loading).toEqual(false);
});

it('should test order by state', () => {
  const orderBy = 'test';
  const state = apiReducer({
    items: apiResponse.items
  }, actions.orderBy(orderBy));

  expect(state.items.length).toEqual(20);
  expect(state.orderBy).toEqual(orderBy);
});

it('should test clear order by state', () => {
  const state = apiReducer({
    items: apiResponse.items
  }, actions.clearOrderBy());

  expect(state.items.length).toEqual(20);
  expect(state.orderBy).toEqual('');
});

