import {API_CONSTANTS} from "../constants";
import _ from 'lodash';

const initialState = {
  items: [],
  error: false,
  loading: false,
  tagName: '',
  orderBy: ''
};

export default function apiReducer(state = initialState, action) {
  switch (action.type) {
    case API_CONSTANTS.TAG_NAME:
      return {
        ...state,
        tagName: action.tagName
      };

    case API_CONSTANTS.START_LOADING:
      return {
        ...state,
        loading: true
      };

    case API_CONSTANTS.FETCH_SUCCESS_ALL_DATA:
      return {
        ...state,
        items: action.payload.items,
        error: initialState.error,
        loading: false,
      };

    case API_CONSTANTS.FETCH_FAIL_ALL_DATA:
      return {
        ...state,
        message: action.message,
        error: true,
        loading: false,
      };

    case API_CONSTANTS.FIND_BY_TAG_SUCCESS:
      return {
        ...state,
        items: action.payload.items,
        loading: false,
        error: false
      };

    case API_CONSTANTS.FIND_BY_TAG_FAIL:
      return {
        ...state,
        message: action.message,
        error: true,
        loading: false,
      };

    case API_CONSTANTS.ORDER_BY:
      return {
        ...state,
        items: _.sortBy(state.items, action.orderBy),
        orderBy: action.orderBy
      };

    case API_CONSTANTS.CLEAR_ORDER_BY:
      return {
        ...state,
        orderBy: initialState.orderBy
      };

    default:
      return state;
  }
}