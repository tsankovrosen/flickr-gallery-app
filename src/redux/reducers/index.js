import api from './api.reducer';
import alert from './alert.reducer';

export default {
  api,
  alert
};