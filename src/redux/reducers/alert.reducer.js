import {ALERT_CONSTANTS} from "../constants";

const initialState = {};

export default function alertReducer(state = initialState, {type, message}) {
  switch (type) {
    case ALERT_CONSTANTS.SUCCESS:
      return {
        type: 'success',
        message
      };

    case ALERT_CONSTANTS.ERROR:
      return {
        type: 'danger',
        message
      };

    case ALERT_CONSTANTS.CLEAR:
      return {};

    default:
      return state;
  }
}