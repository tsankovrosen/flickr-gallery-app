import {ALERT_CONSTANTS} from '../constants';

export {
  successAlert,
  errorAlert,
  clearAlert
}

function successAlert(message) {
  return {
    type: ALERT_CONSTANTS.SUCCESS,
    message
  }
}

function errorAlert(message) {
  return {
    type: ALERT_CONSTANTS.ERROR,
    message
  }
}

function clearAlert() {
  return {
    type: ALERT_CONSTANTS.CLEAR
  }
}