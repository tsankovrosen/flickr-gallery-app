import {API_CONSTANTS} from "../constants";
import {Api} from 'services';
import {errorAlert, clearAlert} from "./alert.actions";

export {
  startLoading,
  setValue,
  orderBy,
  clearOrderBy,
  fetchAll,
};

function startLoading() {
  return {
    type: API_CONSTANTS.START_LOADING
  }
}

function setValue(tagName) {
  return {
    type: API_CONSTANTS.TAG_NAME,
    tagName
  }
}

function orderBy(orderBy) {
  return {
    type: API_CONSTANTS.ORDER_BY,
    orderBy
  }
}

function clearOrderBy() {
  return {
    type: API_CONSTANTS.CLEAR_ORDER_BY
  }
}

function fetchAll(criteria) {
  const success = payload => ({
    type: API_CONSTANTS.FETCH_SUCCESS_ALL_DATA,
    payload
  });

  const failure = message => ({
    type: API_CONSTANTS.FETCH_FAIL_ALL_DATA,
    message
  });

  return async dispatch => {
    dispatch(startLoading());
    dispatch(clearAlert());
    dispatch(clearOrderBy());

    try {
      const {data} = await Api.getAll(criteria);

      if (data.items.length === 0) {
        dispatch(errorAlert('No Records found. Try with something else ...'));
      }

      dispatch(success(data));
    } catch ({response, message, ...rest}) {
      const errorMessage = message;

      dispatch(failure(errorMessage));
      dispatch(errorAlert(errorMessage));
    }
  };
}