export const API_CONSTANTS = {
  START_LOADING: 'START_LOADING',

  FETCH_SUCCESS_ALL_DATA: 'FETCH_SUCCESS_ALL_API_DATA',
  FETCH_FAIL_ALL_DATA: 'FETCH_SUCCESS_ALL_API_DATA',

  TAG_NAME: 'TAG_NAME',

  FIND_BY_TAG_SUCCESS: 'FIND_BY_TAG_SUCCESS',
  FIND_BY_TAG_FAIL: 'FIND_BY_TAG_FAIL',

  ORDER_BY: 'ORDER_BY',
  CLEAR_ORDER_BY: 'CLEAR_ORDER_BY'
};

export const ALERT_CONSTANTS = {
  SUCCESS: 'ALERT_SUCCESS',
  ERROR: 'ALERT_ERROR',
  CLEAR: 'ALERT_CLEAR'
};